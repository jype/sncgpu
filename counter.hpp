#ifndef COUNTER_HPP
#define COUNTER_HPP

#include <stdint.h>

#include <condition_variable>
#include <functional>
#include <iostream>
#include <mutex>
#include <thread>

#include "fixed_queue.hpp"
#include "process.hpp"
#include "sequence.hpp"

template<typename T>
class counter
{
public:
	counter(uint32_t ninput, uint32_t nprocess, uint32_t noutput)
		: ninput(ninput),
		  nprocess(nprocess),
		  noutput(noutput),
		  process_queue(nprocess * 4),
		  output_queue(noutput * 2)
	{
	}

	template<typename Producer, typename Consumer>
	void count(std::vector<Producer>& producers, Consumer& consumer)
	{
		input_finished = false;
		process_finished = false;

		std::vector<std::thread> input_thread;
		std::vector<std::thread> process_thread;
		std::vector<std::thread> output_thread;

		auto produce = &counter<T>::produce_input<Producer>;
		for (uint32_t i = 0; i < producers.size(); ++i)
			input_thread.emplace_back(produce, this,
						  std::ref(producers[i]));

		auto process = &counter<T>::process_input;
		for (uint32_t i = 0; i < nprocess; ++i)
			process_thread.emplace_back(process, this, !i);

		auto consume = &counter<T>::consume_output<Consumer>;
		for (uint32_t i = 0; i < noutput; ++i)
			output_thread.emplace_back(consume, this,
						   std::ref(consumer));

		for (uint32_t i = 0; i < producers.size(); ++i)
			input_thread[i].join();
		input_finished = true;

		process_cond.notify_all();

		for (uint32_t i = 0; i < nprocess; ++i)
			process_thread[i].join();
		process_finished = true;

		output_cond.notify_all();

		for (uint32_t i = 0; i < noutput; ++i)
			output_thread[i].join();
	}
private:
	uint32_t ninput;
	uint32_t nprocess;
	uint32_t noutput;

	bool input_finished;

	bool process_finished;
	std::mutex process_mutex;
	std::condition_variable process_cond;
	fixed_queue<rle_sequence<T> > process_queue;

	std::mutex output_mutex;
	std::condition_variable output_cond;
	fixed_queue<rle_sequence<T> > output_queue;

	template<typename Producer>
	void produce_input(Producer& producer)
	{
		while (true)
		{
			rle_sequence<T> rle_seq = producer.produce();

			if (rle_seq.seq.empty())
				break;

			process_queue.push(std::move(rle_seq));

			process_cond.notify_one();
		}
	}

	void process_input(bool device)
	{
		while (true)
		{
			auto f = [this]{
				return (input_finished ||
					!process_queue.empty());
			};

			std::unique_lock<std::mutex> lock(process_mutex);
			process_cond.wait(lock, f);

			if (process_queue.empty())
			{
				if (input_finished)
					break;
				else
					continue;
			}

			uint32_t size = process_queue.size();

			rle_sequence<T> rle_seq;
			process_queue.pop(rle_seq);

			lock.unlock();

			std::cout << "Process queue: " << size << " " << rle_seq.seq.size() << std::endl;

			if (device)
				process_device(rle_seq.seq, rle_seq.lengths);
			else
				process_host(rle_seq.seq, rle_seq.lengths);

			output_queue.push(std::move(rle_seq));

			output_cond.notify_one();
		}
	}

	template<typename Consumer>
	void consume_output(Consumer& consumer)
	{
		while (true)
		{
			auto f = [this] {
				return (process_finished ||
					!output_queue.empty());
			};

			std::unique_lock<std::mutex> lock(output_mutex);
			output_cond.wait(lock, f);

			if (output_queue.empty())
			{
				if (process_finished)
					break;
				else
					continue;
			}

			uint32_t size = output_queue.size();

			rle_sequence<T> rle_seq;
			output_queue.pop(rle_seq);

			lock.unlock();

			std::cout << "Output queue: " << size << " " << rle_seq.seq.size() << std::endl;

			consumer.consume(rle_seq);
		}

		consumer.finish();
	}
};

#endif
