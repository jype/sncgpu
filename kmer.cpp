#include <stdint.h>
#include <stdlib.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "counter.hpp"
#include "delta_decoder.hpp"
#include "delta_encoder.hpp"
#include "fasta_parser.hpp"
#include "fastpfor_decoder.hpp"
#include "fastpfor_encoder.hpp"
#include "output_writer.hpp"
#include "tmp_reader.hpp"
#include "tmp_writer.hpp"
#include "vlq_decoder.hpp"
#include "vlq_encoder.hpp"

typedef fasta_parser<uint64_t> fparser;
typedef tmp_reader<uint64_t, std::ifstream,
	vlq_decoder, fastpfor_decoder> treader;
typedef tmp_writer<uint64_t, vlq_encoder, fastpfor_encoder> twriter;
typedef output_writer<uint64_t, std::ofstream,
	vlq_encoder, fastpfor_encoder> owriter;

bin_map phase1(const std::vector<std::string>& input,
	       const std::string& tmp, uint64_t work_size,
	       uint32_t k, uint32_t nbins)
{
	std::vector<std::ifstream*> is;
	std::vector<fparser> parsers;

	for (uint32_t i = 0; i < input.size(); ++i)
	{
		is.push_back(new std::ifstream(input[i], std::ios::binary));
		parsers.emplace_back(is[i], nbins, work_size, k);
	}

	twriter tw(tmp);

	counter<uint64_t> c1(1, 1, 2);
	c1.count(parsers, tw);

	for (uint32_t i = 0; i < is.size(); ++i)
		delete is[i];

	return tw.bins;
}

/*void phase2(const std::string& output,*/
/*	    std::queue<std::string>& merge_queue,*/
/*	    uint32_t work_size,*/
/*	    uint32_t k, uint32_t nbins)*/
/*{*/
/*	std::vector<std::ifstream*> isk;*/
/*	std::vector<std::ifstream*> isl;*/

/*	while (!merge_queue.empty())*/
/*	{*/
/*		isk.push_back(new std::ifstream(merge_queue.front() + ".k",*/
/*						 std::ios::binary));*/
/*		isl.push_back(new std::ifstream(merge_queue.front() + ".l",*/
/*						 std::ios::binary));*/
/*		merge_queue.pop();*/
/*	}*/

/*	std::ofstream osk(output + ".k", std::ios::binary);*/
/*	std::ofstream osl(output + ".l", std::ios::binary);*/

/*	std::vector<treader> readers;*/
/*	readers.emplace_back(isk, isl, work_size, 2, k);*/

	/*owriter ow(std::move(osk), std::move(osl));*/
	/*owriter ow(output, nbins);*/

	/*counter<uint64_t> c2(1, 1, 1);*/
	/*c2.count(readers, ow);*/

	/*for (uint32_t i = 0; i < isk.size(); ++i)*/
	/*{*/
	/*	delete isk[i];*/
	/*	delete isl[i];*/
	/*}*/

	/*std::cout << "Merged output to " << output << std::endl;*/
/*}*/

int main(int argc, char* argv[])
{
	uint32_t s;
	uint32_t k;
	uint32_t nbins;

	if (argc < 4)
		return 1;

	s = strtoul(argv[1], NULL, 10);
	k = strtoul(argv[2], NULL, 10);
	nbins = strtoul(argv[3], NULL, 10);

	uint64_t work_size = 1ull << s;

	std::string tmp(argv[4]);
	std::string output(argv[5]);

	std::vector<std::string> input;
	for (int i = 6; i < argc; ++i)
		input.emplace_back(argv[i]);

	bin_map bins = phase1(input, tmp, work_size, k, nbins);

	std::vector<std::ifstream*> isk;
	std::vector<std::ifstream*> isl;

	std::vector<treader> readers;
	for (uint32_t i = 0; i < nbins; ++i)
	{
		if (bins[i].empty())
			continue;

		std::vector<std::ifstream*> isk2;
		std::vector<std::ifstream*> isl2;
		while (!bins[i].empty())
		{
			isk2.push_back(new std::ifstream(bins[i].front() + ".k",
							 std::ios::binary));
			isl2.push_back(new std::ifstream(bins[i].front() + ".l",
							 std::ios::binary));
			bins[i].pop();
		}

		readers.emplace_back(isk2, isl2, work_size, 2, k, i);

		isk.insert(isk.end(), isk2.begin(), isk2.end());
		isl.insert(isl.end(), isl2.begin(), isl2.end());
	}

	/*while (!merge_queue.empty())*/
	/*{*/
	/*	isk.push_back(new std::ifstream(merge_queue.front() + ".k",*/
	/*					 std::ios::binary));*/
	/*	isl.push_back(new std::ifstream(merge_queue.front() + ".l",*/
	/*					 std::ios::binary));*/
	/*	merge_queue.pop();*/
	/*}*/

	/*std::ofstream osk(output + ".k", std::ios::binary);*/
	/*std::ofstream osl(output + ".l", std::ios::binary);*/

	/*readers.emplace_back(isk, isl, work_size, 2, k);*/

	owriter ow(output, nbins);

	counter<uint64_t> c2(1, 1, 1);
	c2.count(readers, ow);

	for (uint32_t i = 0; i < isk.size(); ++i)
	{
		delete isk[i];
		delete isl[i];
	}

	std::cout << "Merged output to " << output << std::endl;

	/*bin_map::iterator it;*/
	/*for (it = bins.begin(); it != bins.end(); ++it)*/
	/*	phase2(output + "." + std::to_string(it->first),*/
	/*	       it->second, work_size, k);*/

	/*uint32_t nmerge = 256;*/

	/*uint32_t merge_count = 1;*/
	/*while (merge_queue.size() > nmerge)*/
	/*{*/
	/*	std::string merge(tmp + "merge." + std::to_string(merge_count));*/

	/*	std::queue<std::string> q;*/
	/*	for (uint32_t i = 0; i < nmerge; ++i)*/
	/*	{*/
	/*		q.push(merge_queue.front());*/
	/*		merge_queue.pop();*/
	/*	}*/

	/*	phase2(merge, q, work_size, k);*/
	/*	merge_queue.push(merge);*/

	/*	++merge_count;*/
	/*}*/

	/*phase2(output, merge_queue, work_size, k);*/

	return 0;
}
