#ifndef DELTA_ENCODER_HPP
#define DELTA_ENCODER_HPP

#include <stdint.h>

#include <ostream>

#include "bit_writer.hpp"
#include "slice.hpp"
#include "utils.hpp"

template<typename T>
class delta_encoder
{
public:
	void write(std::ostream& os, const T* slice, uint32_t ch)
	{
		T len = sizeof(T) * 8 * ch - slice_clz(slice, ch);
		uint32_t lenlen = sizeof(T) * 8 - clz(len) - 1;

		bw.write(os, 0, lenlen);
		bw.write(os, len, lenlen + 1);

		if (!len)
			return;

		len -= 1;
		while (len)
		{
			uint32_t b = ((len - 1) % (sizeof(T) * 8)) + 1;
			bw.write(os, slice[(len - 1) / (sizeof(T) * 8)], b);
			len -= b;
		}
	}

	void flush(std::ostream& os)
	{
		bw.flush(os);
	}
private:
	bit_writer<T> bw;
};

#endif
