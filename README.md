# sncgpu

sncgpu is a GPU accelerated k-mer counter.

## Requirements
A Linux environment with CUDA toolkit is required.
Install CUDA from: https://developer.nvidia.com/cuda-downloads

## Dependencies
```
sudo apt-get install libboost-iostreams-dev
```

## Build FastPFor
```
git submodule update --init
cd external/FastPFor/
cmake .
make
cd -
```

## Build and run sncgpu
```
# Update -gencode parameter in Makefile to match your target GPU(s)
# https://arnon.dk/matching-sm-architectures-arch-and-gencode-for-various-nvidia-cards/

# Build sncgpu
make

# The first parameter is GPU work size in log 2 (27 would be 1 << 27 = 128MB).
# Each worker thread consumes this amount of GPU memory.
# ./kmer <work size> <k-mer length> <workers> <tmpdir> <output> <input...>

# Create temporary directory
mkdir /tmp/work/

# Count 16-mers with 128MB work size and 4 worker threads
./kmer 27 16 4 /tmp/work/ ./out /path/to/file.fasta

# Dump k-mers
./dump ./out.*
```
