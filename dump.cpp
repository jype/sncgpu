#include <stdint.h>

#include <algorithm>
#include <bitset>
#include <fstream>
#include <iostream>
#include <map>
#include <string>

#include "delta_decoder.hpp"
#include "fastpfor_decoder.hpp"
#include "simple_decoder.hpp"
#include "slice.hpp"
#include "utils.hpp"
#include "vlq_decoder.hpp"

template<typename T,
	template<typename> class KmerDecoder,
	typename LengthDecoder>
class sequence_printer
{
public:
	void print(std::istream& isk, std::istream& isl)
	{
		uint32_t s, k;

		isk.read(reinterpret_cast<char*>(&s), sizeof(uint32_t));
		isk.read(reinterpret_cast<char*>(&k), sizeof(uint32_t));

		uint32_t b = CEIL(s * k, sizeof(T) * 8);

		T slice[b] = {0};

		isk.read(reinterpret_cast<char*>(slice), sizeof(T) * b);
		if (isk.gcount() != sizeof(T) * b)
			return;

		std::vector<uint32_t> lengths;
		if (!ldec.read(isl, lengths))
			return;

		uint32_t pos = 0;
		while (true)
		{
			uint32_t& length = lengths[pos++];

			print_slice(slice, s, k, length);

			if (!read_slice(isk, slice, b))
			    break;

			if (pos == lengths.size())
			{
				lengths.clear();

				if (!ldec.read(isl, lengths))
					break;

				pos = 0;
			}
		}
	}
private:
	KmerDecoder<T> kdec;
	LengthDecoder ldec;

	bool read_slice(std::istream& is, T* slice, uint32_t n)
	{
		T diff[n] = {0};

		bool success = kdec.read(is, diff, n);
		slice_add(slice, diff, n);

		return success;
	}

	void print_slice(const T* slice, uint32_t s,
			 uint32_t k, uint32_t length)
	{
		char dict[256] = {0};
		dict[0] = 'A';
		dict[1] = 'C';
		dict[2] = 'G';
		dict[3] = 'T';

		char buf[k + 1];
		for (uint32_t i = k; i; --i)
		{
			uint32_t ch = ((i - 1) * s) / (sizeof(T) * 8);
			uint32_t bit = ((i - 1) * s) % (sizeof(T) * 8);

			T c = (slice[ch] >> bit) & 0x3;

			buf[k - i] = dict[c];
		}

		buf[k] = '\0';

		std::cout << buf << " " << length << std::endl;
	}
};

int main(int argc, char* argv[])
{
	if (!(argc % 2))
		return 1;

	for (uint32_t i = 1; i < argc; i += 2)
	{
		std::ifstream isk(argv[i], std::ios::binary);
		std::ifstream isl(argv[i + 1], std::ios::binary);

		sequence_printer<uint64_t, vlq_decoder, fastpfor_decoder> sp;
		sp.print(isk, isl);

		isk.close();
		isl.close();
	}

	return 0;
}
