FASTPFOR := external/FastPFor
LIBFASTPFOR := $(FASTPFOR)/libFastPFor.a

CPPFLAGS := -I$(FASTPFOR)/headers
CXXFLAGS := -O3 -std=c++11
LDFLAGS := $(LIBFASTPFOR) -lboost_iostreams

NVCC := nvcc
NVCCPPFLAGS := $(CPPFLAGS)
NVCCFLAGS := $(CXXFLAGS) -gencode=arch=compute_30,code=sm_30
NVCCLDFLAGS := $(LDFLAGS)

BIN := kmer
DUMP := dump

SRC :=\
	kmer.cpp \
	process.cu

DUMPSRC :=\
	dump.cpp

HDR :=\
	bit_reader.hpp \
	bit_writer.hpp \
	counter.hpp \
	delta_decoder.hpp \
	delta_encoder.hpp \
	fasta_parser.hpp \
	fastpfor_decoder.hpp \
	fastpfor_encoder.hpp \
	fixed_queue.hpp \
	output_writer.hpp \
	process.hpp \
	sequence.hpp \
	sequence_writer.hpp \
	simple_decoder.hpp \
	simple_encoder.hpp \
	slice.hpp \
	tmp_reader.hpp \
	tmp_writer.hpp \
	utils.hpp \
	vlq_decoder.hpp \
	vlq_encoder.hpp

all: $(BIN) $(DUMP)

$(BIN): $(SRC) $(HDR)
	$(NVCC) $(NVCCFLAGS) $(NVCCPPFLAGS) -o $@ $(SRC) $(NVCCLDFLAGS)

$(DUMP): $(DUMPSRC) $(HDR)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -o $@ $(DUMPSRC) $(LDFLAGS)

clean:
	rm -f $(BIN) $(DUMP)

.PHONY: all clean
