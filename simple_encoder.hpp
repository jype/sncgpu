#ifndef SIMPLE_ENCODER_HPP
#define SIMPLE_ENCODER_HPP

#include <stdint.h>

#include <ostream>

template<typename T>
class simple_encoder
{
public:
	void write(std::ostream& os, const T* slice, uint32_t ch)
	{
		os.write(reinterpret_cast<const char*>(slice),
			 sizeof(T) * ch);
	}

	void flush(std::ostream& os)
	{
	}
};

#endif
