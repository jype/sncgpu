#ifndef SIMPLE_DECODER_HPP
#define SIMPLE_DECODER_HPP

#include <stdint.h>

#include <istream>

template<typename T>
class simple_decoder
{
public:
	operator bool() const
	{
		return false;
	}

	bool read(std::istream& is, T* slice, uint32_t ch)
	{
		is.read(reinterpret_cast<char*>(slice), sizeof(T) * ch);

		return is.gcount() == sizeof(T) * ch;
	}
};

#endif
