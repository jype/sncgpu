#ifndef SLICE_HPP
#define SLICE_HPP

#include <stdint.h>

#include "utils.hpp"

template<typename T>
int slice_compare(const T* lhs, const T* rhs, uint32_t ch)
{
	for (uint32_t i = ch; i; --i)
	{
		const T& a = lhs[i - 1];
		const T& b = rhs[i - 1];

		if (a < b)
			return -1;
		else if (b < a)
			return 1;
	}

	return 0;
}

template<typename T>
void slice_add(T* lhs, const T* rhs, uint32_t ch)
{
	bool carry = false;
	for (uint32_t i = 0; i < ch; ++i)
	{
		T& a = lhs[i];
		T b = rhs[i];
		T tmp = a + b + carry;
		carry = tmp < a;
		a = tmp;
	}
}

template<typename T>
void slice_subtract(T* lhs, const T* rhs, uint32_t ch)
{
	bool carry = true;
	for (uint32_t i = 0; i < ch; ++i)
	{
		T& a = lhs[i];
		const T& b = rhs[i];
		T tmp = a + ~b + carry;
		carry = b <= a;
		a = tmp;
	}
}

template<typename T>
void slice_reverse_subtract(T* lhs, const T* rhs, uint32_t ch)
{
	bool carry = true;
	for (uint32_t i = 0; i < ch; ++i)
	{
		T& a = lhs[i];
		const T& b = rhs[i];
		T tmp = ~a + b + carry;
		carry = a <= b;
		a = tmp;
	}
}

template<typename T>
void slice_shift_left(T* slice, uint32_t ch, uint32_t n)
{
	if (n < sizeof(T) * 8)
	{
		for (uint32_t i = ch - 1; i; --i)
		{
			slice[i] <<= n;
			slice[i] |= slice[i - 1] >> (sizeof(T) * 8 - n);
		}

		slice[0] <<= n;
	}
	else
	{
		for (uint32_t i = ch - 1; i; --i)
			slice[i] = slice[i - 1];
		slice[0] = 0;
	}
}

template<typename T>
void slice_shift_right(T* slice, uint32_t ch, uint32_t n)
{
	if (n < sizeof(T) * 8)
	{
		for (uint32_t i = 0; i < ch - 1; ++i)
		{
			slice[i] >>= n;
			slice[i] |= slice[i + 1] << (sizeof(T) * 8 - n);
		}

		slice[ch - 1] >>= n;
	}
	else
	{
		for (uint32_t i = 0; i < ch - 1; ++i)
			slice[i] = slice[i + 1];
		slice[ch - 1] = 0;
	}
}

template<typename T>
void slice_push(T* slice, uint32_t ch, uint32_t s, T c)
{
	slice_shift_left(slice, ch, s);
	slice[0] |= c;
}

template<typename T>
void slice_mask(T* slice, uint32_t ch, uint32_t n)
{
	T mask = (~(T)0) >> ((sizeof(T) * 8) - (n % (sizeof(T) * 8)));

	if (mask)
		slice[ch - 1] &= mask;
}

template<typename T>
uint8_t slice_msb(const T* slice, uint32_t ch, uint32_t n)
{
	T msb = slice[ch - 1];

	if (n > 8)
	{
		uint32_t shift = (n - 8) % (sizeof(T) * 8);

		if (ch > 1 && shift)
		{
			msb <<= sizeof(T) * 8 - shift;
			msb |= slice[ch - 2] >> shift;
		}
		else
		{
			msb >>= shift;
		}
	}

	return msb & 0xff;
}

template<typename T>
uint32_t slice_clz(const T* slice, uint32_t ch)
{
	uint32_t b = 0;
	for (uint32_t i = ch; i; --i)
	{
		const T& t = slice[i - 1];
		if (!t)
			b += sizeof(T) * 8;
		else
			return b + clz(t);
	}

	return b;
}

#endif
