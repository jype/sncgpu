#ifndef FIXED_QUEUE_HPP
#define FIXED_QUEUE_HPP

#include <stdint.h>

#include <condition_variable>
#include <mutex>
#include <queue>

template<typename T>
class fixed_queue
{
public:
	uint32_t queue_size;

	fixed_queue(uint32_t size = 1)
		: queue_size(size)
	{
	}

	bool empty() const
	{
		std::lock_guard<std::mutex> lock(queue_mutex);

		return q.empty();
	}

	uint32_t size() const
	{
		std::lock_guard<std::mutex> lock(queue_mutex);

		return q.size();
	}

	const T& front() const
	{
		return q.front();
	}

	void push(T&& item)
	{
		std::unique_lock<std::mutex> queue_lock(queue_mutex);

		while (q.size() >= queue_size)
			queue_cond.wait(queue_lock);

		q.emplace(std::move(item));
	}

	void pop(T& item)
	{
		std::unique_lock<std::mutex> queue_lock(queue_mutex);

		item = std::move(q.front());
		q.pop();

		queue_lock.unlock();

		queue_cond.notify_one();
	}

	T&& pop()
	{
		std::unique_lock<std::mutex> queue_lock(queue_mutex);

		T item = std::move(q.front());
		q.pop();

		queue_lock.unlock();

		queue_cond.notify_one();

		return item;
	}
private:
	std::queue<T> q;

	mutable std::mutex queue_mutex;
	std::condition_variable queue_cond;
};

#endif
