#ifndef TMP_WRITER_HPP
#define TMP_WRITER_HPP

#include <stdint.h>

#include <atomic>
#include <fstream>
#include <iostream>
#include <map>
#include <mutex>
#include <queue>
#include <string>

#include "sequence.hpp"
#include "sequence_writer.hpp"

typedef std::map<uint32_t, std::queue<std::string> > bin_map;

template<typename T,
	template<typename> class KmerEncoder,
	typename LengthEncoder>
class tmp_writer
{
public:
	bin_map bins;

	tmp_writer(const std::string& path)
		: path(path),
		  output_count(1)
	{
	}

	void consume(const rle_sequence<T>& rle_seq)
	{
		std::string bin = std::to_string(rle_seq.bin);
		std::string count = std::to_string(output_count++);

		std::string output = path + "tmp." + bin + "." + count;

		write_sequence(output + ".k", rle_seq.seq);
		write_lengths(output + ".l", rle_seq.lengths);

		std::cout << "Wrote output to " << output << std::endl;

		std::unique_lock<std::mutex> lock(merge_mutex);
		bins[rle_seq.bin].push(output);
		lock.unlock();
	}

	void finish()
	{
	}
private:
	std::string path;

	std::mutex merge_mutex;
	std::atomic<uint32_t> output_count;

	void write_sequence(const std::string& path, const sequence<T>& seq)
	{
		std::ofstream os(path, std::ios::binary);

		sequence_writer<T, KmerEncoder> sw;
		sw.write(os, seq);

		sw.flush(os);

		os.close();
	}

	void write_lengths(const std::string& path,
			   const std::vector<uint32_t>& lengths)
	{
		std::ofstream os(path, std::ios::binary);

		LengthEncoder enc;
		enc.write(os, lengths);

		os.close();
	}
};

#endif
