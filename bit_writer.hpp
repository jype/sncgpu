#ifndef BIT_WRITER_HPP
#define BIT_WRITER_HPP

#include <stdint.h>

#include <algorithm>
#include <ostream>

template<typename T, uint32_t N = 1024>
class bit_writer
{
public:
	bit_writer()
		: cur(0),
		  available(N),
		  left(sizeof(T) * 8)
	{
	}

	void write(std::ostream& os, T bits, uint32_t n)
	{
		while (n)
		{
			uint32_t len = std::min(left, n);
			T mask = (~(T)0) >> (sizeof(T) * 8 - len);

			if (len < sizeof(T) * 8)
				cur <<= len;
			else
				cur = 0;

			if (n - len < sizeof(T) * 8)
				cur |= (bits >> (n - len)) & mask;

			left -= len;
			n -= len;

			if (!left)
			{
				push(os);

				if (!available)
					flush(os);
			}
		}
	}

	void flush(std::ostream& os)
	{
		if (available < N)
		{
			os.write(reinterpret_cast<const char*>(buf),
				 (N - available) * sizeof(T));

			available = N;
		}

		if (left < sizeof(T) * 8)
		{
			cur <<= left;
			os.write(reinterpret_cast<const char*>(&cur),
				 sizeof(T));
			cur = 0;
			left = sizeof(T) * 8;
		}

	}
private:
	T buf[N];
	T cur;
	uint32_t available;
	uint32_t left;

	void push(std::ostream& os)
	{
		buf[N - available] = cur;
		--available;

		cur = 0;
		left = sizeof(T) * 8;
	}
};

#endif
