#ifndef OUTPUT_WRITER_HPP
#define OUTPUT_WRITER_HPP

#include "sequence.hpp"
#include "sequence_writer.hpp"

template<typename T,
	typename OutputStream,
	template<typename> class KmerEncoder,
	typename LengthEncoder>
class output_writer
{
public:
	output_writer(const std::string& path, uint32_t nbins)
		: sw(nbins),
		  enc(nbins),
		  output_finished(false),
		  write_mutex(nbins),
		  write_cond(nbins),
		  write_queue(nbins)
	{
		auto write = &output_writer<T, OutputStream, KmerEncoder, LengthEncoder>::write;
		for (uint32_t i = 0; i < nbins; ++i)
		{
			std::string output = path + "." + std::to_string(i);

			osk.emplace_back(output + ".k", std::ios::binary);
			osl.emplace_back(output + ".l", std::ios::binary);

			write_queue[i].queue_size = 2;
			write_thread.emplace_back(write, this, i);
		}
	}

	/*output_writer(OutputStream&& osk, OutputStream&& osl)*/
	/*	: osk(std::move(osk)),*/
	/*	  osl(std::move(osl))*/
	/*{*/
	/*}*/

	/*~output_writer()*/
	/*{*/
		/*sw.flush(osk);*/
	/*}*/

	void consume(rle_sequence<T>& rle_seq)
	{
		write_queue[rle_seq.bin].push(std::move(rle_seq));
		write_cond[rle_seq.bin].notify_one();
		/*sw.write(osk, rle_seq.seq);*/
		/*enc.write(osl, rle_seq.lengths);*/
	}

	void finish()
	{
		output_finished = true;

		for (uint32_t i = 0; i < write_thread.size(); ++i)
			write_cond[i].notify_all();

		for (uint32_t i = 0; i < write_thread.size(); ++i)
			write_thread[i].join();
	}
private:
	std::vector<std::ofstream> osk;
	std::vector<std::ofstream> osl;
	std::vector<sequence_writer<T, KmerEncoder> > sw;
	std::vector<LengthEncoder> enc;

	bool output_finished;
	std::vector<std::thread> write_thread;
	std::vector<std::mutex> write_mutex;
	std::vector<std::condition_variable> write_cond;
	std::vector<fixed_queue<rle_sequence<T> > > write_queue;

	void write(uint32_t bin)
	{
		while (true)
		{
			auto f = [this, bin] {
				return (output_finished ||
					!write_queue[bin].empty());
			};

			std::unique_lock<std::mutex> lock(write_mutex[bin]);
			write_cond[bin].wait(lock, f);

			if (write_queue[bin].empty())
			{
				if (output_finished)
					break;
				else
					continue;
			}

			uint64_t size = write_queue[bin].size();

			rle_sequence<T> rle_seq;
			write_queue[bin].pop(rle_seq);

			lock.unlock();

			std::cout << "Output queue: " << size << " " << rle_seq.seq.size() << std::endl;

			sw[bin].write(osk[bin], rle_seq.seq);
			enc[bin].write(osl[bin], rle_seq.lengths);
		}

		sw[bin].flush(osk[bin]);
	}
};

#endif
