#ifndef FASTA_PARSER_HPP
#define FASTA_PARSER_HPP

#include <stdint.h>

#include <algorithm>
#include <istream>
#include <string>
#include <vector>

/*#include <boost/iostreams/filtering_stream.hpp>*/
/*#include <boost/iostreams/device/file.hpp>*/
/*#include <boost/iostreams/filter/bzip2.hpp>*/
/*#include <boost/iostreams/filter/gzip.hpp>*/

#include "sequence.hpp"
#include "utils.hpp"

/*namespace io = boost::iostreams;*/

template<typename T>
class fasta_parser
{
public:
	fasta_parser(std::istream* is, uint32_t nbins,
		     uint64_t work_size, uint32_t k)
		: available(0),
		  pos(0),
		  is(is),
		  nbins(nbins),
		  nbinslog2(0),
		  spos(1),
		  work_size(work_size),
		  k(k),
		  ch(CEIL(2 * k, sizeof(T) * 8))
	{
		std::fill(dict, dict + 256, 255);

		dict['A'] = 0;
		dict['a'] = 0;
		dict['C'] = 1;
		dict['c'] = 1;
		dict['G'] = 2;
		dict['g'] = 2;
		dict['T'] = 3;
		dict['t'] = 3;

		for (uint32_t i = nbins >> 1; i; i >>= 1)
			++nbinslog2;

		/*if (ends_with(path, ".bz2"))*/
		/*	in.push(io::bzip2_decompressor());*/
		/*else if (ends_with(path, ".gz"))*/
		/*	in.push(io::gzip_decompressor());*/

		/*in.push(io::file_source(path));*/

		for (uint32_t i = 0; i < nbins; ++i)
			bins.emplace_back(2, k);

		slice.resize(ch);
	}

	rle_sequence<T> produce()
	{
		rle_sequence<T> rle_seq;
		uint32_t bin;

		if (parse(bin))
			goto success;

		for (uint32_t i = 0; i < bins.size(); ++i)
		{
			if (!bins[i].empty())
			{
				bin = i;
				goto success;
			}
		}

		return rle_seq;
success:
		rle_seq.seq = std::move(bins[bin]);
		rle_seq.lengths.resize(rle_seq.seq.size(), 1);
		rle_seq.bin = bin;

		bins[bin] = sequence<T>(2, k);

		return rle_seq;
	}
private:
	static constexpr uint32_t buffer_size = 8192;
	uint8_t buf[buffer_size];
	uint32_t available;
	uint32_t pos;

	uint8_t dict[256];

	std::istream* is;

	std::vector<sequence<T> > bins;
	uint32_t nbins;
	uint32_t nbinslog2;

	std::vector<T> slice;
	uint32_t spos;

	uint64_t work_size;

	uint32_t k;
	uint32_t ch;

	bool parse(uint32_t& bin)
	{
		while (*is || available)
		{
			if (!available)
			{
				is->read(reinterpret_cast<char*>(buf),
					 buffer_size * sizeof(uint8_t));
				available = is->gcount() / sizeof(uint8_t);
				pos = 0;
			}
			else
			{
				--available;

				uint8_t c = buf[pos++];

				if (c == '>')
				{
					skip();
					spos = 1;
					continue;
				}

				if (dict[c] == 255)
					continue;

				slice_push(slice.data(), ch, 2, (T)dict[c]);

				if (spos < k)
				{
					++spos;
				}
				else
				{
					slice_mask(slice.data(), ch, 2 * k);

					uint8_t msb = slice_msb(slice.data(), ch, 2 * k);
					msb >>= (8 - nbinslog2);

					bins[msb].push(slice.data());

					if (bins[msb].bsize() >= work_size)
					{
						bin = msb;
						return true;
					}
				}
			}
		}

		return false;
	}

	void skip()
	{
		while (*is || available)
		{
			if (!available)
			{
				is->read(reinterpret_cast<char*>(buf),
					 buffer_size * sizeof(uint8_t));
				available = is->gcount() / sizeof(uint8_t);
				pos = 0;
			}
			else
			{
				--available;
				if (buf[pos++] == '\n')
					return;
			}
		}
	}
};

#endif
