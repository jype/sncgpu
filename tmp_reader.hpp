#ifndef TMP_READER_HPP
#define TMP_READER_HPP

#include <stdint.h>

#include <algorithm>
#include <istream>
#include <stdexcept>
#include <vector>

#include "sequence.hpp"
#include "slice.hpp"
#include "utils.hpp"

template<typename T,
	typename InputStream,
	template<typename> class KmerDecoder,
	typename LengthDecoder>
	class tmp_reader
{
	public:
		tmp_reader(std::vector<InputStream*>& isk,
			   std::vector<InputStream*>& isl,
			   uint64_t work_size, uint32_t s,
			   uint32_t k, uint32_t bin)
			: isk(isk),
			  isl(isl),
			  work_size(work_size),
			  n(this->isk.size()),
			  s(s),
			  k(k),
			  ch(CEIL(s * k, sizeof(T) * 8)),
			  bin(bin),
			  first(true)
	{
		kdec.resize(n);
		ldec.resize(n);
		index.resize(n);

		seq.resize(n);
		lengths.resize(n);
		slice.resize(n);

		for (uint32_t i = 0; i < n; ++i)
		{
			seq[i] = sequence<T>(s, k);
			slice[i].resize(ch);
		}
	}

		operator bool() const
		{
			return n;
		}

		rle_sequence<T> produce()
		{
			uint64_t seq_size = work_size / (ch * sizeof(T));

			if (first)
			{
				init();
				first = false;
			}

			if (!read_input() && bsize() < work_size)
			{
				const uint32_t im = index[0];
				const uint64_t min_size = seq[im].size();

				if (lengths[im].size() < min_size)
					throw std::runtime_error("length mismatch");

				sequence<T> output_seq(seq[im]);
				std::vector<uint32_t> output_lengths(lengths[im].begin(),
								     lengths[im].begin() + min_size);

				seq[im].clear_all();
				lengths[im].erase(lengths[im].begin(),
						  lengths[im].begin() + min_size);

				uint64_t out_size = output_seq.size();

				output_seq.resize(seq_size);
				output_lengths.resize(seq_size);

				for (uint32_t i = 1; i < n; ++i)
				{
					const uint32_t ii = index[i];

					if (seq[ii].size() < lengths[ii].size())
						throw std::runtime_error("length mismatch");

					output_seq.copy(seq[ii], out_size, seq[ii].size());
					std::copy(lengths[ii].begin(),
						  lengths[ii].begin() + lengths[ii].size(),
						  output_lengths.begin() + out_size);

					out_size += seq[ii].size();

					lengths[ii].erase(lengths[ii].begin(),
							  lengths[ii].begin() + seq[ii].size());
					seq[ii].clear_all();
				}

				output_seq.resize(out_size);
				output_lengths.resize(out_size);

				rle_sequence<T> rle_seq;
				rle_seq.seq = std::move(output_seq);
				rle_seq.lengths = std::move(output_lengths);
				rle_seq.bin = bin;

				return rle_seq;
			}

			for (int32_t i = n - 1; i >= 0; --i)
			{
				const uint32_t ii = index[i];

				if (!seq[ii].empty() && lengths[ii].size() >= seq[ii].size())
					continue;

				index.erase(index.begin() + i);
				--n;
			}

			if (!n)
			{
				rle_sequence<T> rle_seq;
				rle_seq.bin = bin;

				return rle_seq;
			}

			uint32_t min = 0;
			for (uint32_t i = 1; i < n; ++i)
			{
				const uint32_t ii = index[i];

				if (seq[ii].compare(seq[index[min]]) < 0)
					min = i;
			}

			const uint32_t im = index[min];
			const uint64_t min_size = seq[im].size();

			if (lengths[im].size() < min_size)
				throw std::runtime_error("length mismatch");

			sequence<T> output_seq(seq[im]);
			std::vector<uint32_t> output_lengths(lengths[im].begin(),
							     lengths[im].begin() + min_size);

			uint64_t out_size = output_seq.size();

			output_seq.resize(seq_size);
			output_lengths.resize(seq_size);

			for (uint32_t i = 0; i < n; ++i)
			{
				const uint32_t ii = index[i];

				if (ii == im)
					continue;

				uint64_t j;
				for (j = 0; j < seq[ii].size(); ++j)
				{
					if (seq[im].compare(seq[ii], j) < 0)
						break;
				}

				if (!j)
					continue;

				output_seq.copy(seq[ii], out_size, j);
				std::copy(lengths[ii].begin(),
					  lengths[ii].begin() + j,
					  output_lengths.begin() + out_size);

				out_size += j;

				lengths[ii].erase(lengths[ii].begin(),
						  lengths[ii].begin() + j);
				seq[ii].erase(j);
			}

			seq[im].clear_all();
			lengths[im].erase(lengths[im].begin(),
					  lengths[im].begin() + min_size);

			output_seq.resize(out_size);
			output_lengths.resize(out_size);

			rle_sequence<T> rle_seq;
			rle_seq.seq = std::move(output_seq);
			rle_seq.lengths = std::move(output_lengths);
			rle_seq.bin = bin;

			return rle_seq;
		}
	private:
		std::vector<InputStream*> isk;
		std::vector<InputStream*> isl;
		std::vector<KmerDecoder<T> > kdec;
		std::vector<LengthDecoder> ldec;
		std::vector<uint32_t> index;

		std::vector<sequence<T> > seq;
		std::vector<std::vector<uint32_t> > lengths;
		std::vector<std::vector<T> > slice;

		uint64_t work_size;

		uint32_t n;
		uint32_t s;
		uint32_t k;
		uint32_t ch;
		uint32_t bin;

		bool first;

		void init()
		{
			for (uint32_t i = 0; i < n; ++i)
			{
				uint32_t s2, k2;

				isk[i]->read(reinterpret_cast<char*>(&s2),
					     sizeof(uint32_t));
				isk[i]->read(reinterpret_cast<char*>(&k2),
					     sizeof(uint32_t));

				if (!*isk[i] || s != s2 || k != k2)
					throw std::runtime_error("bad stream");
			}

			for (int32_t i = n - 1; i >= 0; --i)
			{
				index[i] = i;

				const uint32_t& ii = index[i];
				T* v = &slice[ii][0];

				isk[ii]->read(reinterpret_cast<char*>(v),
					      ch * sizeof(T));

				if (isk[ii]->gcount() == ch * sizeof(T))
				{
					seq[ii].push(v);
				}
				else
				{
					index.erase(index.begin() + i);
					--n;

					continue;
				}

				if (!ldec[ii].read(*isl[ii], lengths[ii]))
				{
					index.erase(index.begin() + i);
					--n;
				}
			}
		}

		bool read_input()
		{
			uint32_t left = n;

			uint64_t seq_size = work_size / (ch * sizeof(T));
			for (uint32_t i = 0; i < n; ++i)
			{
				const uint32_t ii = index[i];
				uint32_t eof = 0;

				seq[ii].reserve(seq_size / n);

				uint64_t j = seq[ii].size();
				for (; j * n < seq_size; ++j)
				{
					if (!read_slice(*isk[ii], kdec[ii],
							&slice[ii][0], ch))
					{
						++eof;
						break;
					}

					seq[ii].push(&slice[ii][0]);
				}

				if (isl[ii]->peek() == EOF)
				{
					++eof;
				}
				else
				{
					while (lengths[ii].size() < seq[ii].size())
					{
						if (!ldec[ii].read(*isl[ii], lengths[ii]))
						{
							++eof;
							break;
						}
					}
				}

				if (eof == 2)
					--left;
			}

			return left;
		}

		bool read_slice(std::istream& is, KmerDecoder<T>& dec,
				T* slice, uint32_t n)
		{
			T diff[n] = {0};

			bool success = dec.read(is, diff, n);
			slice_add(slice, diff, n);

			return success;
		}

		uint64_t bsize() const
		{
			uint64_t size = 0;

			for (uint32_t i = 0; i < n; ++i)
			{
				const uint32_t ii = index[i];

				size += seq[ii].size() * ch * sizeof(T);
			}

			return size;
		}
};

#endif
