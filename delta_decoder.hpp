#ifndef DELTA_DECODER_HPP
#define DELTA_DECODER_HPP

#include <stdint.h>

#include <istream>

#include "bit_reader.hpp"

template<typename T>
class delta_decoder
{
public:
	operator bool() const
	{
		return br;
	}

	bool read(std::istream& is, T* slice, uint32_t ch)
	{
		uint32_t c = br.zeros(is);
		T len = 0;

		if (!br.read(is, &len, 1, c + 1) || !len)
			return false;

		slice[0] = 1;

		return br.read(is, slice, ch, len - 1);
	}
private:
	bit_reader<T> br;
};

#endif
