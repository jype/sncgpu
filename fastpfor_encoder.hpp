#ifndef FASTPFOR_ENCODER_HPP
#define FASTPFOR_ENCODER_HPP

#include <stdint.h>

#include <algorithm>
#include <memory>

#include "codecfactory.h"

using namespace FastPForLib;

class fastpfor_encoder
{
public:
	fastpfor_encoder()
		: codec(CODECFactory::getFromName("simple8b"))
	{
	}

	template<typename T>
	void write(std::ostream& os, const std::vector<T>& v)
	{
		const uint32_t* p = static_cast<const uint32_t*>(v.data());

		uint32_t n = v.size() * sizeof(T) / sizeof(uint32_t);
		for (uint32_t i = 0; i < n; i += block_size)
		{
			size_t decoded_size = std::min<size_t>(block_size,
							       n - i);
			size_t encoded_size = block_size * 2;

			std::vector<uint32_t> output(encoded_size);
			codec->encodeArray(p + i, decoded_size,
					   output.data(), encoded_size);

			uint32_t size = decoded_size;
			os.write(reinterpret_cast<const char*>(&size),
				 sizeof(uint32_t));

			size = encoded_size;
			os.write(reinterpret_cast<const char*>(&size),
				 sizeof(uint32_t));

			os.write(reinterpret_cast<const char*>(output.data()),
				 size * sizeof(uint32_t));
		}
	}
private:
	static constexpr uint32_t block_size = 1024;

	std::shared_ptr<IntegerCODEC> codec;
};

#endif
