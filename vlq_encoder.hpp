#ifndef VLQ_ENCODER_HPP
#define VLQ_ENCODER_HPP

#include <stdint.h>

#include <algorithm>
#include <ostream>

#include "slice.hpp"

template<typename T>
class vlq_encoder
{
public:
	vlq_encoder()
		: pos(0)
	{
	}

	void write(std::ostream& os, const T* slice, uint32_t ch)
	{
		T tmp[ch];

		std::copy(slice, slice + ch, tmp);

		uint32_t bits = ch * sizeof(T) * 8 - slice_clz(tmp, ch);
		uint32_t bytes = (bits + 6) / 7;

		if (pos + bytes >= buffer_size)
			flush(os);

		pos += bytes;

		for (uint32_t i = 0; i < bytes; ++i)
		{
			buf[pos - i - 1] = (tmp[0] & 0x7f) | 0x80;
			slice_shift_right(tmp, ch, 7);
		}

		buf[pos - 1] ^= 0x80;
	}

	void flush(std::ostream& os)
	{
		os.write(reinterpret_cast<const char*>(buf),
			 pos * sizeof(uint8_t));
		pos = 0;
	}
private:
	static constexpr uint32_t buffer_size = 8192;
	uint8_t buf[buffer_size];
	uint32_t pos;
};

#endif
