#ifndef SEQUENCE_WRITER_HPP
#define SEQUENCE_WRITER_HPP

#include <stdint.h>

#include <ostream>
#include <vector>

#include "sequence.hpp"
#include "slice.hpp"

template<typename T, template<typename> class Encoder>
class sequence_writer
{
public:
	sequence_writer()
		: first(true)
	{
	}

	void write(std::ostream& os, const sequence<T>& seq)
	{
		uint32_t ch = seq.channels.size();
		T tmp[ch];

		if (first)
		{
			init(os, seq);
			last.resize(ch);

			first = false;
		}
		else
		{
			for (uint32_t i = 0; i < ch; ++i)
				tmp[i] = seq.channels[i][0];

			slice_subtract(tmp, &last[0], ch);
			enc.write(os, tmp, ch);
		}

		uint64_t seq_size = seq.size();
		for (uint64_t i = 1; i < seq_size; ++i)
		{
			seq.diff(tmp, i);
			enc.write(os, tmp, ch);
		}

		for (uint32_t i = 0; i < ch; ++i)
			last[i] = seq.channels[i][seq_size - 1];
	}

	void flush(std::ostream& os)
	{
		enc.flush(os);
	}
private:
	Encoder<T> enc;
	std::vector<T> last;
	bool first;

	void init(std::ostream& os, const sequence<T>& seq)
	{
		uint32_t ch = seq.channels.size();
		T tmp[ch];

		os.write(reinterpret_cast<const char*>(&seq.s),
			 sizeof(uint32_t));
		os.write(reinterpret_cast<const char*>(&seq.k),
			 sizeof(uint32_t));

		for (uint32_t i = 0; i < ch; ++i)
			tmp[i] = seq.channels[i][0];

		os.write(reinterpret_cast<const char*>(tmp), ch * sizeof(T));
	}
};

#endif
