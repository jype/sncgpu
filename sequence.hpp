#ifndef SEQUENCE_HPP
#define SEQUENCE_HPP

#include <stdint.h>

#include <algorithm>
#include <vector>

#include "utils.hpp"

template<typename T>
class sequence
{
public:
	std::vector<std::vector<T> > channels;
	uint32_t s;
	uint32_t k;

	sequence()
		: s(0),
		  k(0),
		  pos(0)
	{
	}

	sequence(uint32_t s, uint32_t k)
		: channels(CEIL(s * k, sizeof(T) * 8)),
		  s(s),
		  k(k),
		  pos(0)
	{
	}

	bool empty() const
	{
		return !size();
	}

	uint64_t size() const
	{
		if (pos && pos < k)
		{
			if (channels[0].empty())
				return 0;
			else
				return channels[0].size() - 1;
		}
		else if (!channels.empty())
		{
			return channels[0].size();
		}

		return 0;
	}

	uint64_t bsize() const
	{
		return channels.size() * size() * sizeof(T);
	}

	void reset()
	{
		if (pos && pos < k)
		{
			if (!channels[0].empty())
			{
				for (uint32_t i = 0; i < channels.size(); ++i)
					channels[i].pop_back();
			}
		}

		pos = 0;
	}

	void push(T c)
	{
		shift();

		/* TODO: apply mask to c */
		T& elem = channels[0].back();
		elem = (elem << s) | c;

		T mask = (~(T)0) >> ((sizeof(T) * 8) - ((s * k) % (sizeof(T) * 8)));

		if (mask)
			channels.back().back() &= mask;
	}

	void push(T* slice)
	{
		for (uint32_t i = 0; i < channels.size(); ++i)
			channels[i].push_back(slice[i]);
	}

	void shift()
	{
		if (!pos)
		{
			for (uint32_t i = 0; i < channels.size(); ++i)
				channels[i].push_back(0);
		}
		else if (pos == k)
		{
			for (uint32_t i = 0; i < channels.size(); ++i)
				channels[i].push_back(channels[i].back());
		}

		if (pos < k)
			++pos;

		for (uint32_t i = channels.size() - 1; i; --i)
		{
			T& elem = channels[i].back();
			elem <<= s;
			elem |= channels[i - 1].back() >> (sizeof(T) * 8 - s);
		}
	}

	sequence<T> tail() const
	{
		sequence<T> seq(s, k);

		if (!channels[0].empty())
		{
			seq.pos = pos;
			if (seq.pos == k)
				--seq.pos;

			for (uint32_t i = 0; i < channels.size(); ++i)
				seq.channels[i].push_back(channels[i].back());
		}

		return seq;
	}

	void clear()
	{
		if (!channels[0].empty())
		{
			if (pos == k)
				--pos;

			for (uint32_t i = 0; i < channels.size(); ++i)
			{
				channels[i][0] = channels[i].back();
				channels[i].resize(1);
			}
		}
	}

	void clear_all()
	{
		for (uint32_t i = 0; i < channels.size(); ++i)
			channels[i].clear();
	}

	void diff(T* slice, uint64_t pos) const
	{
		bool carry = true;
		for (uint32_t i = 0; i < channels.size(); ++i)
		{
			const T& a = channels[i][pos];
			const T& b = channels[i][pos - 1];
			T& c = slice[i];
			c = a + ~b + carry;
			carry = b <= a;
		}
	}

	void delta()
	{
		for (uint64_t i = size() - 1; i; --i)
		{
			bool carry = true;
			for (uint32_t j = 0; j < channels.size(); ++j)
			{
				T& a = channels[j][i];
				T b = ~channels[j][i - 1];
				T tmp = a + b + carry;
				carry = tmp <= a;
				a = tmp;
			}
		}
	}

	int compare(T* slice, uint64_t pos) const
	{
		for (uint32_t i = channels.size(); i; --i)
		{
			const T& a = channels[i - 1][pos];
			const T& b = slice[i - 1];

			if (a < b)
				return -1;
			else if (b < a)
				return 1;
		}

		return 0;
	}

	int compare(const sequence<T>& other) const
	{
		for (uint32_t i = channels.size(); i; --i)
		{
			const T& a = channels[i - 1].back();
			const T& b = other.channels[i - 1].back();

			if (a < b)
				return -1;
			else if (b < a)
				return 1;
		}

		return 0;
	}

	int compare(const sequence<T>& other, uint64_t pos) const
	{
		for (uint32_t i = channels.size(); i; --i)
		{
			const T& a = channels[i - 1].back();
			const T& b = other.channels[i - 1][pos];

			if (a < b)
				return -1;
			else if (b < a)
				return 1;
		}

		return 0;
	}

	void copy(const sequence<T>& other, uint64_t pos, uint32_t n)
	{
		for (uint32_t i = 0; i < channels.size(); ++i)
			std::copy(other.channels[i].begin(),
				  other.channels[i].begin() + n,
				  channels[i].begin() + pos);
	}

	void insert(const sequence<T>& other, uint32_t n)
	{
		for (uint32_t i = 0; i < channels.size(); ++i)
			channels[i].insert(channels[i].end(),
					   other.channels[i].begin(),
					   other.channels[i].begin() + n);
	}

	void erase(uint64_t n)
	{
		for (uint32_t i = 0; i < channels.size(); ++i)
			channels[i].erase(channels[i].begin(),
					  channels[i].begin() + n);
	}

	void reserve(uint64_t n)
	{
		for (uint32_t i = 0; i < channels.size(); ++i)
			channels[i].reserve(n);
	}

	void resize(uint64_t n)
	{
		for (uint32_t i = 0; i < channels.size(); ++i)
			channels[i].resize(n);
	}
private:
	uint32_t pos;
};

template<typename T>
class rle_sequence
{
public:
	sequence<T> seq;
	std::vector<uint32_t> lengths;
	uint32_t bin;
};

#endif
