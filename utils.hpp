#ifndef UTILS_HPP
#define UTILS_HPP

#include <limits.h>
#include <stdint.h>

#include <string>

#define CEIL(a, b) (((a) + (b) - 1) / (b))

template<typename T>
inline uint32_t clz(T t)
{
	uint32_t c;

	for (c = sizeof(T) * 8; t; --c, t >>= 1);

	return c;
}

template<>
inline uint32_t clz(uint32_t t)
{
#if UINT_MAX == UINT32_MAX
	return __builtin_clz(t);
#else
	return __builtin_clzl(t);
#endif
}

template<>
inline uint32_t clz(uint64_t t)
{
#if ULONG_MAX == UINT64_MAX
	return __builtin_clzl(t);
#else
	return __builtin_clzll(t);
#endif
}

inline bool ends_with(const std::string& str, const std::string& suffix)
{
	if (str.size() < suffix.size())
		return false;

	return !(str.compare(str.size() - suffix.size(),
			     suffix.size(), suffix));
}

#endif
