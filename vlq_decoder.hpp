#ifndef VLQ_DECODER_HPP
#define VLQ_DECODER_HPP

#include <stdint.h>

#include <istream>

#include "slice.hpp"

template<typename T>
class vlq_decoder
{
public:
	vlq_decoder()
		: available(0),
		  pos(0)
	{
	}

	operator bool() const
	{
		return available;
	}

	bool read(std::istream& is, T* slice, uint32_t ch)
	{
		while (is || available)
		{
			if (!available)
			{
				is.read(reinterpret_cast<char*>(buf),
					buffer_size * sizeof(uint8_t));
				available = is.gcount() / sizeof(uint8_t);
				pos = 0;
			}
			else
			{
				slice_shift_left(slice, ch, 7);
				slice[0] |= buf[pos] & 0x7f;

				--available;

				if (!(buf[pos++] & 0x80))
					return true;
			}
		}

		return false;
	}
private:
	static constexpr uint32_t buffer_size = 8192;
	uint8_t buf[buffer_size];
	uint32_t available;
	uint32_t pos;
};

#endif
