#ifndef FASTPFOR_DECODER_HPP
#define FASTPFOR_DECODER_HPP

#include <stdint.h>

#include <memory>

#include "codecfactory.h"

using namespace FastPForLib;

class fastpfor_decoder
{
public:
	fastpfor_decoder()
		: codec(CODECFactory::getFromName("simple8b"))
	{
	}

	template<typename T>
	bool read(std::istream& is, std::vector<T>& v)
	{
		uint32_t decoded_size;
		is.read(reinterpret_cast<char*>(&decoded_size),
			sizeof(uint32_t));

		if (is.gcount() != sizeof(uint32_t))
			return false;

		uint32_t encoded_size;
		is.read(reinterpret_cast<char*>(&encoded_size),
			sizeof(uint32_t));

		if (is.gcount() != sizeof(uint32_t))
			return false;

		std::vector<uint32_t> encoded(encoded_size);
		is.read(reinterpret_cast<char*>(encoded.data()),
			encoded_size * sizeof(uint32_t));

		if (is.gcount() != encoded_size * sizeof(uint32_t))
			return false;

		uint32_t n = decoded_size * sizeof(uint32_t) / sizeof(T);

		std::vector<T> decoded(n);
		uint32_t* p = static_cast<uint32_t*>(decoded.data());

		size_t size = decoded_size;

		codec->decodeArray(encoded.data(), encoded_size, p, size);

		if (size != decoded_size)
			return false;

		v.insert(v.end(), decoded.begin(), decoded.end());

		return true;
	}
private:
	std::shared_ptr<IntegerCODEC> codec;
};

#endif
