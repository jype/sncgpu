#ifndef PROCESS_HPP
#define PROCESS_HPP

#include <stdint.h>

#include <vector>

#include "sequence.hpp"

void process_host(sequence<uint64_t>& seq, std::vector<uint32_t>& lengths);
void process_device(sequence<uint64_t>& seq, std::vector<uint32_t>& lengths);

#endif
