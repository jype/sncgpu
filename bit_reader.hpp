#ifndef BIT_READER_HPP
#define BIT_READER_HPP

#include <stdint.h>

#include <algorithm>
#include <istream>

#include "slice.hpp"
#include "utils.hpp"

template<typename T, uint32_t N = 1024>
class bit_reader
{
public:
	bit_reader()
		: cur(0),
		  available(0),
		  left(0),
		  pos(0)
	{
	}

	operator bool() const
	{
		return available || left;
	}

	uint32_t zeros(std::istream& is)
	{
		uint32_t n = 0;

		while (is || available || left)
		{
			if (!left)
			{
				pop(is);
			}
			else if (!cur)
			{
				n += left;
				left = 0;
			}
			else
			{
				uint32_t len = clz(cur);

				cur <<= len;
				left -= len;
				n += len;

				break;
			}
		}

		return n;
	}

	bool read(std::istream& is, T* slice, uint32_t ch, uint32_t n)
	{
		if (!n)
			return true;

		while ((is || available || left) && n)
		{
			if (!left)
			{
				pop(is);
			}
			else
			{
				uint32_t len = std::min(left, n);

				slice_shift_left(slice, ch, len);
				slice[0] |= cur >> (sizeof(T) * 8 - len);

				if (len < sizeof(T) * 8)
					cur <<= len;
				else
					cur = 0;

				left -= len;
				n -= len;
			}
		}

		return !n;
	}
private:
	T buf[N];
	T cur;
	uint32_t available;
	uint32_t left;
	uint32_t pos;

	void pop(std::istream& is)
	{
		if (!available)
		{
			is.read(reinterpret_cast<char*>(buf), N * sizeof(T));
			available = is.gcount() / sizeof(T);
			pos = 0;
		}

		if (available)
		{
			cur = buf[pos];
			--available;
			left = sizeof(T) * 8;
			++pos;
		}
	}
};

#endif
