#include <stdint.h>

#include <algorithm>
#include <bitset>
#include <numeric>
#include <vector>

#include <thrust/adjacent_difference.h>
#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/gather.h>
#include <thrust/host_vector.h>
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <thrust/unique.h>
#include <thrust/iterator/constant_iterator.h>

#include "sequence.hpp"

template<typename KeyVector, typename PermutationVector>
void update_permutation(KeyVector& keys, PermutationVector& permutation)
{
	// Temporary storage for keys
	KeyVector tmp(keys.size());

	// Permute the keys with the current reordering
	thrust::gather(permutation.begin(), permutation.end(),
		       keys.begin(), tmp.begin());

	// Stable sort the permuted keys and update the permutation
	thrust::stable_sort_by_key(tmp.begin(), tmp.end(),
				   permutation.begin());
}

template<typename KeyVector, typename PermutationVector>
void apply_permutation(KeyVector& keys, PermutationVector& permutation)
{
	// Copy keys to temporary vector
	KeyVector tmp(keys.begin(), keys.end());

	// Permute the keys
	thrust::gather(permutation.begin(), permutation.end(),
		       tmp.begin(), keys.begin());
}

template<typename KeyVector, typename ValueVector>
void sort_sequence(std::vector<KeyVector>& seq,
		   ValueVector& lengths, uint64_t n)
{
	if (seq.size() == 1)
	{
		// Regular sort_by_key is sufficient
		thrust::sort_by_key(seq[0].begin(), seq[0].end(),
				    lengths.begin());
		return;
	}

	KeyVector permutation(n);
	thrust::sequence(permutation.begin(), permutation.end());

	/* Get permutation vector for sequence */
	for (uint32_t i = 0; i < seq.size(); ++i)
		update_permutation(seq[i], permutation);

	/* Apply permutation to each channel */
	for (uint32_t i = 0; i < seq.size(); ++i)
		apply_permutation(seq[i], permutation);

	/* Apply permutation to run lengths */
	apply_permutation(lengths, permutation);
}

template<typename Vector>
Vector scan_sequence(std::vector<Vector>& seq, uint64_t n)
{
	typedef typename Vector::value_type T;

	Vector scan(n);
	Vector combined(n);

	thrust::adjacent_difference(seq[0].begin(), seq[0].end(),
				    combined.begin(),
				    thrust::not_equal_to<T>());
	combined[0] = 1;

	for (uint32_t i = 1; i < seq.size(); ++i)
	{
		thrust::adjacent_difference(seq[i].begin(), seq[i].end(),
					    scan.begin(),
					    thrust::not_equal_to<T>());

		thrust::transform(scan.begin(), scan.end(),
				  combined.begin(), combined.begin(),
				  thrust::logical_or<T>());
	}

	return combined;
}

template<typename KeyVector, typename ValueVector>
uint64_t run_length_encode(std::vector<KeyVector>& seq,
			   ValueVector& lengths, uint64_t n)
{
	typedef typename KeyVector::value_type T;

	if (seq.size() == 1)
	{
		ValueVector tmp(lengths);
		return thrust::reduce_by_key(seq[0].begin(), seq[0].end(),
					     tmp.begin(), seq[0].begin(),
					     lengths.begin()).first - seq[0].begin();
	}

	KeyVector combined(scan_sequence(seq, n));

	KeyVector indices(n);
	thrust::sequence(indices.begin(), indices.end());

	thrust::transform(indices.begin(), indices.end(),
			  combined.begin(), indices.begin(),
			  thrust::multiplies<T>());

	uint64_t runs = thrust::remove(indices.begin() + 1,
				       indices.end(), 0) - indices.begin();

	for (uint32_t i = 0; i < seq.size(); ++i)
	{
		KeyVector tmp(seq[i].begin(), seq[i].end());
		thrust::gather(indices.begin(), indices.begin() + runs,
			       tmp.begin(), seq[i].begin());
	}

	thrust::inclusive_scan(combined.begin(), combined.end(),
			       combined.begin());

	ValueVector tmp(lengths);
	thrust::reduce_by_key(combined.begin(), combined.end(),
			      tmp.begin(), indices.begin(),
			      lengths.begin());

	return runs;
}

template<typename T, template<typename...> typename Vector>
void process(sequence<T>& seq, std::vector<uint32_t>& lengths)
{
	uint64_t n = seq.size();
	uint32_t ch = seq.channels.size();

	std::vector<Vector<T> > seqv(ch);
	Vector<uint32_t> lengthsv(lengths);

	for (uint32_t i = 0; i < ch; ++i)
		seqv[i] = Vector<T>(seq.channels[i].begin(),
				    seq.channels[i].begin() + n);

	sort_sequence(seqv, lengthsv, n);
	uint64_t runs = run_length_encode(seqv, lengthsv, n);

	for (uint32_t i = 0; i < ch; ++i)
	{
		thrust::copy(seqv[i].begin(), seqv[i].begin() + runs,
			     seq.channels[i].begin());
		seq.channels[i].resize(runs);
	}

	lengths.resize(runs);
	thrust::copy(lengthsv.begin(), lengthsv.begin() + runs, lengths.begin());
}

void process_host(sequence<uint64_t>& seq, std::vector<uint32_t>& lengths)
{
	process<uint64_t, thrust::host_vector>(seq, lengths);
}

void process_device(sequence<uint64_t>& seq, std::vector<uint32_t>& lengths)
{
	process<uint64_t, thrust::device_vector>(seq, lengths);
}
